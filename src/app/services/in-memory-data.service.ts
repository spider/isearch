import { InMemoryDbService } from 'angular-in-memory-web-api';
import { MediaItem } from './mediaItem';

export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const items = [
      { authors: {}, title: 'Mr. Nice', type: 'DOC' },
      { authors: {}, title: 'Narco', type: 'DOC' },
      { authors: {}, title: 'Bombasto', type: 'DOC' },
      { authors: {}, title: 'Celeritas', type: 'DOC' },
      { authors: {}, title: 'Magneta', type: 'DOC' },
      { authors: {}, title: 'RubberMan', type: 'ALBUM' },
      { authors: {}, title: 'Dynama', type: 'ALBUM' },
      { authors: {}, title: 'Dr IQ', type: 'ALBUM' },
      { authors: {}, title: 'Magma', type: 'ALBUM' },
      { authors: {}, title: 'Tornado', type: 'ALBUM' }
    ];
    return {items: items};
  }
}
