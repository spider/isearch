import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

import {Observable, of} from 'rxjs';
import {catchError} from 'rxjs/operators';

import {MediaItem} from './mediaItem';

@Injectable({providedIn: 'root'})
export class MediaItemService {

  private appUrl = 'http://localhost:8080/media';

  constructor(
    private http: HttpClient) {
  }

  searchItems(term: string): Observable<MediaItem[]> {
    if (!term.trim()) {
      return of([]);
    }
    return this.http.get<MediaItem[]>(`${this.appUrl}/?term=${term}`).pipe(
      catchError(this.handleError<MediaItem[]>('searchItems', []))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error); // log to console instead
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
