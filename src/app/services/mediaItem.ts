export class MediaItem {
  title: string;
  authors: string[];
  type: string;
}
