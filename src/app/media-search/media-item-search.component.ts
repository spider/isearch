import { Component, OnInit } from '@angular/core';

import { Observable, Subject } from 'rxjs';

import {
   debounceTime, distinctUntilChanged, switchMap
 } from 'rxjs/operators';

import { MediaItem } from '../services/mediaItem';
import { MediaItemService } from '../services/media-item.service';

@Component({
  selector: 'app-media-item-search',
  templateUrl: './media-item-search.component.html',
  styleUrls: [ './media-item-search.component.css' ]
})
export class MediaItemSearchComponent implements OnInit {
  columns = ['title', 'authors', 'type'];
  items$: Observable<MediaItem[]>;
  private searchTerms = new Subject<string>();

  constructor(private mediaItemService: MediaItemService) {}

  // Push a search term into the observable stream.
  search(term: string): void {
    this.searchTerms.next(term);
  }

  ngOnInit(): void {
    this.items$ = this.searchTerms.pipe(
      // wait 300ms after each keystroke before considering the term
      debounceTime(100),

      // ignore new term if same as previous term
      distinctUntilChanged(),

      // switch to new search observable each time the term changes
      switchMap((term: string) => this.mediaItemService.searchItems(term)),
    );
  }
}
