import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MediaItemSearchComponent } from './media-search/media-item-search.component';

const routes: Routes = [
  { path: '', redirectTo: '/mediasearch', pathMatch: 'full' },
  { path: 'mediasearch', component: MediaItemSearchComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
